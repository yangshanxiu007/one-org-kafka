package main

import (
    "fmt"
	"strconv"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"errors"
	"strings"
	"bytes"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

const (
	apiHost            = "Magento API URL"
	magentoAccessToken = "Magento bearer token"
)


// ProductContract contract for handling BasicAssets
type ProductContract struct {
	contractapi.Contract
}


func (s *ProductContract) Init(ctx CustomTransactionContextInterface, A string, Aval int, B string, Bval int) error {
	fmt.Println("ProductContract Init")
	var err error
	fmt.Printf("Aval = %d, Bval = %d\n", Aval, Bval)
	err = ctx.GetStub().PutState(A, []byte(strconv.Itoa(Aval)))
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(B, []byte(strconv.Itoa(Bval)))
	if err != nil {
		return err
	}
	return nil
}

func (s *ProductContract) InitProducts(ctx CustomTransactionContextInterface) error {
	fmt.Println("Calling magento product api")
	request, _ := http.NewRequest(`GET`, apiHost+`products?searchCriteria={"sortOrders":[{"field":"id","direction":"asc"}]}`, nil)
	request.Header.Set("Authorization", magentoAccessToken)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return fmt.Errorf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		// fmt.Println(string(data))
		var responseObject ProductResponse
		json.Unmarshal(data, &responseObject)
		for i := 0; i < len(responseObject.Items); i++ {
			fmt.Println(responseObject.Items[i].Sku)
			responseObject.Items[i].ObjectType = "product"
			itemAsBytes, _ := json.Marshal(responseObject.Items[i])
			key := "P" + strconv.Itoa(responseObject.Items[i].ID)
			err = ctx.GetStub().PutState(key, itemAsBytes)
			if err != nil {
				return fmt.Errorf("failed to put to world state. %v", err)
			}
		}
	}
	return nil
}

// NewAsset adds a new basic asset to the world state using id as key
func (s *ProductContract) PutProduct(ctx CustomTransactionContextInterface, productID string, productItem Item) error {
	existing := ctx.GetData()

	if existing != nil {
		return fmt.Errorf("Cannot create new basic asset in world state as key %s already exists", productID)
	}

	productBytes, _ := json.Marshal(productItem)

	err := ctx.GetStub().PutState(productID, []byte(productBytes))

	if err != nil {
		return errors.New("Unable to interact with world state")
	}

	return nil
}

func (s *ProductContract) QueryProduct(ctx CustomTransactionContextInterface, productID string) (string, error) {
	// Get the state from the ledger
	itemAsBytes := ctx.GetData()
	if itemAsBytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + productID + "\"}"
		return "", errors.New(jsonResp)
	}
	fmt.Println(len(productID))
	fmt.Println(len(itemAsBytes))

	fmt.Println(string(itemAsBytes))
	jsonResp := "{\"ID\":\"" + productID + "\",\"sku\":\"" + string(itemAsBytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return string(itemAsBytes), nil
}

/* uat attribute_set_id 21
*/
func (s *ProductContract) CreateProduct(ctx CustomTransactionContextInterface, sku string, attribute_set_id string) error {
    var err error
	sku = strings.Replace(sku, " ", "+", -1)
	productBody := []byte(`
	{
		"product": {
		  "sku": "` + sku  + `",
		  "name": "` + sku  + `",
		  "attribute_set_id": "` + attribute_set_id  + `"
		}
	  }
	`)
	request, _ := http.NewRequest("POST", apiHost+"products", bytes.NewBuffer(productBody))
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", magentoAccessToken)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return fmt.Errorf("The HTTP request failed with error. %v", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		var item Item
		json.Unmarshal(data, &item)

		if item.Sku != sku {
			fmt.Println("Invalid SKU")
			return fmt.Errorf("Invalid SKU")
		}
		item.ObjectType = "product"
		key := "P" + strconv.Itoa(item.ID)
		itemAsBytes, _ := json.Marshal(item)
		err = ctx.GetStub().PutState(key, itemAsBytes)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *ProductContract) QueryAllProducts(ctx CustomTransactionContextInterface) (string, error) {
	startKey := "P0"
	endKey := "P999999"

	resultsIterator, err := ctx.GetStub().GetStateByRange(startKey, endKey)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for queryAllProducts\"}"
		fmt.Println(err)
		return "", errors.New(jsonResp)
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			jsonResp := "{\"Error\":\"resultsIterator\"}"
			return "", errors.New(jsonResp)
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString(`{"Key":`)
		buffer.WriteString(`"`)
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString(`"`)

		buffer.WriteString(`, "Record":`)
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString(`}`)
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString(`]`)

	fmt.Printf("- queryAllProducts:\n%s\n", buffer.String())

	return string(buffer.Bytes()), nil
}

func (s *ProductContract) DeleteProduct(ctx CustomTransactionContextInterface, productID string) error {
	productItem := ctx.GetData()
	if productItem == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + productID + "\"}"
		return errors.New(jsonResp)
	}
	// Delete the key from the state in ledger
	err := ctx.GetStub().DelState(productID)
	if err != nil {
		return fmt.Errorf("Failed to delete state")
	}
	return nil
}

// GetEvaluateTransactions returns functions of ProductContract not to be tagged as submit
func (cc *ProductContract) GetEvaluateTransactions() []string {
	return []string{"GetAsset"}
}