Terminal 1
docker rm -f $(docker ps -aq) && docker volume prune
cd fabric-samples/chaincode-docker-devmode
docker-compose -f docker-compose-simple.yaml up

Terminal 2
cd fabric-samples/chaincode/magento/go
/usr/local/go/bin/go mod init github.com/hyperledger/fabric-samples/chaincode/magento/go
/usr/local/go/bin/go get -u github.com/hyperledger/fabric-contract-api-go

docker exec -it chaincode sh
cd magento/go
go mod vendor
go build -o magento
CORE_CHAINCODE_ID_NAME=magento:0 CORE_PEER_TLS_ENABLED=false ./magento -peer.address peer:7052

Terminal 3
docker exec -it cli sh
peer chaincode install -p chaincodedev/chaincode/magento/go -n magento -v 0
peer chaincode instantiate -n magento -v 0 -c '{"Args":[]}' -C myc

//new
#ProductContract
//Init for test
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:Init","A","1","B","2"]}' -C myc

//InitProducts Get all magento skus save to fabric
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:InitProducts","key"]}' -C myc

//Query product with ID
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:QueryProduct","P651"]}' -C myc

//"{\"docType\":\"product\",\"id\":651,\"sku\":\"test444\",\"name\":\"test444\",\"attribute_set_id\":21,\"price\":0,\"status\":1,\"visibility\":4,\"type_id\":\"simple\",\"created_at\":\"2020-10-13 03:35:30\",\"updated_at\":\"2020-10-13 03:35:30\"}" 

//Put product
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:PutProduct","P651","{\"docType\":\"product\",\"id\":651,\"sku\":\"test444\",\"name\":\"test444\",\"attribute_set_id\":21,\"price\":0,\"status\":1,\"visibility\":4,\"type_id\":\"simple\",\"created_at\":\"2020-10-13 03:35:30\",\"updated_at\":\"2020-10-13 03:35:30\"}"]}' -C myc
//Need add all params, otherwise there are following error
//Error: endorsement failure during invoke. response: status:500 message:"Error managing parameter param1. Value did not match schema:\n1. param1: status is required\n2. param1: visibility is required" 

//Query all products
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:QueryAllProducts","key"]}' -C myc

//Delete product from fabric with ID
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:DeleteProduct","P651"]}' -C myc

//Create product on magento save to fabric with sku's name and attribute set ID
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:CreateProduct","test333","21"]}' -C myc

Tips: All args need add key, otherwise there is following error
Error: endorsement failure during invoke. response: status:500 message:"Missing key for world state" 

//new
#OrderContract
//Init for test
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:Init","A","1","B","2"]}' -C myc

//Start Orders
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:InitOrders", "key"]}' -C myc

/Query Commands
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:QueryOrder","O29109"]}' -C myc
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:QueryAllOrders", "key"]}' -C myc

//Delete Commands
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:DeleteOrder","29109"]}' -C myc