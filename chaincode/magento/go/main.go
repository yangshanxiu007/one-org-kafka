package main

import (
    "github.com/hyperledger/fabric-contract-api-go/contractapi"
)

func main() {
	productContract := new(ProductContract)
	productContract.TransactionContextHandler = new(CustomTransactionContext)
	productContract.BeforeTransaction = GetWorldState
	productContract.UnknownTransaction = UnknownTransactionHandler

	orderContract := new(OrderContract)
	orderContract.TransactionContextHandler = new(CustomTransactionContext)
	orderContract.BeforeTransaction = GetWorldState

    productContract.Name = "org.example.com.ProductContract"
    orderContract.Name = "org.example.com.OrderContract"
    
	cc, err := contractapi.NewChaincode(productContract, orderContract)

	if err != nil {
		panic(err.Error())
	}

	if err := cc.Start(); err != nil {
		panic(err.Error())
	}
}