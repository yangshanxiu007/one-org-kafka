package main

type ProductResponse struct {
	Items  []Item `json:"items"`
	Number int    `json:"total_count"`
}

//Item is the structure of the products stored in API
type Item struct {
	ObjectType    string  `json:"docType"`
	ID            int     `json:"id"`
	Sku           string  `json:"sku"`
	Name          string  `json:"name"`
	AtributeSetID int     `json:"attribute_set_id"`
	Price         float32 `json:"price"`
	Status        int     `json:"status"`
	Visibility    int     `json:"visibility"`
	TypeID        string  `json:"type_id"`
	CreatedAt     string  `json:"created_at"`
	UpdatedAt     string  `json:"updated_at"`
}

type OrderResponse struct {
	Orders []Order `json:"items"`
	Number int     `json:"total_count"`
}

type Order struct {
	ObjectType         string          `json:docType`
	EntityID           int             `json:"entity_id"`
	CreatedAt          string          `json:"created_at"`
	CustomerID         int             `json:"customer_id"`
	GlobalCurrencyCode string          `json:"global_currency_code"`
	GrandTotal         float32         `json:"grand_total"`
	IncrementId        string          `json:"increment_id"`
	ShippingAmount     float32         `json:"shipping_amount"`
	State              string          `json:"state"`
	Status             string          `json:"status"`
	Subtotal           float32         `json:"subtotal"`
	totalorderCount    int             `json:"total_order_count"`
	UpdatedAt          string          `json:"updated_at"`
	orders             []order         `json:"orders"`
}

type order struct {
	CreatedAt   string  `json:"created_at"`
	Description string  `json:"description"`
	orderID     int     `json:"order_id"`
	Name        string  `json:"name"`
	Price       float32 `json:"price"`
	ProductID   int     `json:"product_id"`
	QtyInvoiced int     `json:"qty_invoiced"`
	QntOrdered  int     `json:"qty_ordered"`
	Sku         string  `json:"sku"`
	StoreId     int     `json:"store_id"`
	UpdatedAt   string  `json:"updated_at"`
}