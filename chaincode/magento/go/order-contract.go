package main

import (
    "fmt"
	"strconv"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"errors"
	"bytes"

    "github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// OrderContract contract for handling writing and reading from the world state
type OrderContract struct {
    contractapi.Contract
}

func (s *OrderContract) Init(ctx CustomTransactionContextInterface, A string, Aval int, B string, Bval int) error {
	fmt.Println("MagentoContract Order Init")
	var err error
	fmt.Printf("Aval = %d, Bval = %d\n", Aval, Bval)
	err = ctx.GetStub().PutState(A, []byte(strconv.Itoa(Aval)))
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(B, []byte(strconv.Itoa(Bval)))
	if err != nil {
		return err
	}
	return nil
}


func (s *OrderContract) InitOrders(ctx CustomTransactionContextInterface) error {
	fmt.Println("Calling magento orders api")
	request, _ := http.NewRequest(`GET`, apiHost+`orders?searchCriteria[pageSize]=100&searchCriteria[currentPage]=1&searchCriteria[sortOrders][0][field]=entity_id&searchCriteria[sortOrders][0][direction]=desc`, nil)
	request.Header.Set("Authorization", magentoAccessToken)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return fmt.Errorf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		// fmt.Println(string(data))
		var responseObject OrderResponse
		json.Unmarshal(data, &responseObject)
		for i := 0; i < len(responseObject.Orders); i++ {
			fmt.Println(responseObject.Orders[i].EntityID)
			responseObject.Orders[i].ObjectType = "orders"
			orderAsBytes, _ := json.Marshal(responseObject.Orders[i])
			key := "O" + strconv.Itoa(responseObject.Orders[i].EntityID)
			err = ctx.GetStub().PutState(key, orderAsBytes)
			if err != nil {
				return fmt.Errorf("failed to put to world state. %v", err)
			}
		}
	}
	return nil
}

func (s *OrderContract) QueryOrder(ctx CustomTransactionContextInterface, orderID string) (string, error) {
	// Get the state from the ledger
	orderAsBytes := ctx.GetData()
	if orderAsBytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + orderID + "\"}"
		return "", errors.New(jsonResp)
	}
	fmt.Println(len(orderID))
	fmt.Println(len(orderAsBytes))

	fmt.Println(string(orderAsBytes))
	jsonResp := "{\"ID\":\"" + orderID + "\",\"order\":\"" + string(orderAsBytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return string(orderAsBytes), nil
}

func (s *OrderContract) QueryAllOrders(ctx CustomTransactionContextInterface) (string, error) {
	startKey := "O0"
	endKey := "O999999"

	resultsIterator, err := ctx.GetStub().GetStateByRange(startKey, endKey)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for queryAllOrders\"}"
		fmt.Println(err)
		return "", errors.New(jsonResp)
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			jsonResp := "{\"Error\":\"resultsIterator\"}"
			return "", errors.New(jsonResp)
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString(`{"Key":`)
		buffer.WriteString(`"`)
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString(`"`)

		buffer.WriteString(`, "Record":`)
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString(`}`)
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString(`]`)

	fmt.Printf("- queryAllOrders:\n%s\n", buffer.String())

	return string(buffer.Bytes()), nil
}

func (s *OrderContract) DeleteOrder(ctx CustomTransactionContextInterface, orderID string) error {
	key := "O" + orderID
	// Delete the key from the state in ledger
	err := ctx.GetStub().DelState(key)
	if err != nil {
		return fmt.Errorf("Failed to delete state")
	}
	return nil
}
