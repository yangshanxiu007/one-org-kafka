# Magento objects

## Defining an object
This magento tutorial will create a contract which handles the management of an object to show how the contract API handles the taking and returning of non-string types.

Create a new file in your `magento/go` folder called `basic-asset.go`. In here we will create the object to manage, in this case we will call it `ProductResponse`,`OrderResponse`. 

```
type ProductResponse struct {
	Items  []Item `json:"items"`
	Number int    `json:"total_count"`
}

//Item is the structure of the products stored in API
type Item struct {
	ObjectType    string  `json:"docType"`
	ID            int     `json:"id"`
	Sku           string  `json:"sku"`
	Name          string  `json:"name"`
	AtributeSetID int     `json:"attribute_set_id"`
	Price         float32 `json:"price"`
	Status        int     `json:"status"`
	Visibility    int     `json:"visibility"`
	TypeID        string  `json:"type_id"`
	CreatedAt     string  `json:"created_at"`
	UpdatedAt     string  `json:"updated_at"`
}

type OrderResponse struct {
	Orders []Order `json:"items"`
	Number int     `json:"total_count"`
}

type Order struct {
	ObjectType         string          `json:docType`
	EntityID           int             `json:"entity_id"`
	CreatedAt          string          `json:"created_at"`
	CustomerID         int             `json:"customer_id"`
	GlobalCurrencyCode string          `json:"global_currency_code"`
	GrandTotal         float32         `json:"grand_total"`
	IncrementId        string          `json:"increment_id"`
	ShippingAmount     float32         `json:"shipping_amount"`
	State              string          `json:"state"`
	Status             string          `json:"status"`
	Subtotal           float32         `json:"subtotal"`
	totalorderCount    int             `json:"total_order_count"`
	UpdatedAt          string          `json:"updated_at"`
	orders             []order         `json:"orders"`
}

type order struct {
	CreatedAt   string  `json:"created_at"`
	Description string  `json:"description"`
	orderID     int     `json:"order_id"`
	Name        string  `json:"name"`
	Price       float32 `json:"price"`
	ProductID   int     `json:"product_id"`
	QtyInvoiced int     `json:"qty_invoiced"`
	QntOrdered  int     `json:"qty_ordered"`
	Sku         string  `json:"sku"`
	StoreId     int     `json:"store_id"`
	UpdatedAt   string  `json:"updated_at"`
}
```

Notice that the struct properties are tagged with JSON tags. When a call is made to chaincode created using the contractapi package the transaction arguments and returned values are strings which get converted to and from their go value by a serializer. By default this serializer is a JSON serializer which is built on top of the standard JSON marshalling/unmarshalling in Go, it is also possible to use a serializer of your own definition. These tags are therefore used to tell the serializer how to convert to and from the object. In this case it says the property 'ID' is referenced in a JSON string by the property 'id'. These JSON tags are also used in the metadata to describe the object, more detail can be found in the [godoc](https://godoc.org/github.com/hyperledger/fabric-contract-api-go/metadata#GetSchema). This is as the metadata is intended to tell a user of the smart contract what they need to send in a transaction and what to expect in return.

> Note: as the default serializer is built on top of the standard JSON marshalling/unmarshalling in Go, it is possible to write your own handler for the marshalling by creating MarshalJSON and UnmarshalJSON functions. Beware you may need to make use of the `metadata` tag in your struct to ensure that the contract metadata matches this custom setup.

## Building a contract to handle an object

Now that the object is defined, create a new contract to handle it. This contract will handle the business logic of managing our basic asset. This contract can be created in the same way as the simple contract was. Start by creating a new file `product-contract.go` and add a struct `ProductContract` which embeds the `contractapi.Contract` struct.

```
package main

import (
    "encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// ProductContract contract for handling ProductResponse
type ProductContract struct {
	contractapi.Contract
}
```

## Adding a second contract to the chaincode

In `main.go` file you now need to add code to use your product and order contract inside the chaincode. This is done using the exact same method as the order contract by creating a new instance of the `OrderContract` struct and passing this new instance as an argument to the `contractapi.NewChaincode` function. Earlier in this tutorial you programmed the order contract to make use of the custom transaction context and rely on `GetWorldState` being called before each transaction. Like with the product contract you must let the chaincode know to use these. Your main function should therefore look like this:

```
func main() {
	productContract := new(ProductContract)
	productContract.TransactionContextHandler = new(CustomTransactionContext)
	productContract.BeforeTransaction = GetWorldState
	productContract.UnknownTransaction = UnknownTransactionHandler

	orderContract := new(OrderContract)
	orderContract.TransactionContextHandler = new(CustomTransactionContext)
	orderContract.BeforeTransaction = GetWorldState

    productContract.Name = "org.example.com.ProductContract"
    orderContract.Name = "org.example.com.OrderContract"
    
	cc, err := contractapi.NewChaincode(productContract, orderContract)

	if err != nil {
		panic(err.Error())
	}

	if err := cc.Start(); err != nil {
		panic(err.Error())
	}
}
```

You now have a chaincode consisting of two contracts.

> Note: since both contracts are part of the same chaincode they can read and write to the same keys in the ledger.

### Product contract

```
//Init for test
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:Init","A","1","B","2"]}' -C myc

//InitProducts Get all magento skus save to fabric
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:InitProducts","key"]}' -C myc

//Query product with ID
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:QueryProduct","P651"]}' -C myc

//"{\"docType\":\"product\",\"id\":651,\"sku\":\"test444\",\"name\":\"test444\",\"attribute_set_id\":21,\"price\":0,\"status\":1,\"visibility\":4,\"type_id\":\"simple\",\"created_at\":\"2020-10-13 03:35:30\",\"updated_at\":\"2020-10-13 03:35:30\"}" 

//Put product
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:PutProduct","P651","{\"docType\":\"product\",\"id\":651,\"sku\":\"test444\",\"name\":\"test444\",\"attribute_set_id\":21,\"price\":0,\"status\":1,\"visibility\":4,\"type_id\":\"simple\",\"created_at\":\"2020-10-13 03:35:30\",\"updated_at\":\"2020-10-13 03:35:30\"}"]}' -C myc
//Need add all params, otherwise there are following error
//Error: endorsement failure during invoke. response: status:500 message:"Error managing parameter param1. Value did not match schema:\n1. param1: status is required\n2. param1: visibility is required" 

//Query all products
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:QueryAllProducts","key"]}' -C myc

//Delete product from fabric with ID
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:DeleteProduct","P651"]}' -C myc

//Create product on magento save to fabric with sku's name and attribute set ID
peer chaincode invoke -n magento -c '{"Args":["org.example.com.ProductContract:CreateProduct","test333","21"]}' -C myc
```

### Order contract

> You can call the order contract both using its name or by just passing the name of its functions since it is now the default

```
#OrderContract
//Init for test
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:Init","A","1","B","2"]}' -C myc

//Start Orders
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:InitOrders", "key"]}' -C myc

/Query Commands
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:QueryOrder","O29109"]}' -C myc
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:QueryAllOrders", "key"]}' -C myc

//Delete Commands
peer chaincode invoke -n magento -c '{"Args":["org.example.com.OrderContract:DeleteOrder","29109"]}' -C myc
```