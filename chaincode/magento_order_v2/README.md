## Magento Orders Chaincode

This are chaincodes that initialise and manage the ledger based on the magento orders api.

### Requirements

- Hyperledger Fabric set-up completed with all dependencies installed.
- Magento backend set up. You will need to have the access tokens for magento

### Setting up the chaincode

- Edit the chaincode to add in the `apiHost` and `magentoAccessToken`

```go
const (
	apiHost            = "Magento API URL"
	magentoAccessToken = "Magento bearer token"
)
```



- You will need to install the chain code on all nodes

```bash
docker exec cli peer chaincode install -n magento_order_v2 -v 1.0 -p github.com/chaincode/magento_order_v2/go
```

- Instantiate it on any one of the nodes.

```bash
docker exec cli peer chaincode instantiate -o orderer0.example.com:7050 -C mychannel -n magento_order_v2 -v 1.0 -c '{"Args":[]}' --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```

- Initialise the ledger. (This should only be called once)

```bash
docker exec cli peer chaincode invoke -o orderer0.example.com:7050 -C mychannel -n magento_order_v2 -c '{"Args":["InitOrders"]}' --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```

### Testing the chaincode

After setting up the chaincode, you can test the different functions available:

#### Query
entity_id
- queryOrder(entity_id)

```bash
docker exec cli peer chaincode query -C mychannel -n magento_order_v2 -c '{"Args":["QueryOrder", "{entity_id}"]}'
```

- queryAllOrders()

```bash
docker exec cli peer chaincode query -C mychannel -n magento_order_v2 -c '{"Args":["QueryAllOrders"]}'
```

#### Invoke

- deleteOrder(entity_id)

```bash
docker exec cli peer chaincode invoke -o orderer0.example.com:7050 -C mychannel -n magento_order_v2 -c '{"Args":["DeleteOrder","{entity_id}"]}' --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```

