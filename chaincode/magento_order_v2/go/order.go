/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The sample smart contract for documentation topic:
 * Writing Your First Blockchain Application
 */

 package main

 /* Imports
  * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
  * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
  */
 import (
	"fmt"
	"strconv"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"errors"
	"bytes"

    "github.com/hyperledger/fabric-contract-api-go/contractapi"
 )
 
 const (
	 apiHost            = "Magento API URL"
	 magentoAccessToken = "Magento bearer token"
 )
 
 // Define the Smart Contract structure
 type MagentoContract struct {
	contractapi.Contract
 }
 
 type OrderResponse struct {
	 Orders []Order `json:"items"`
	 Number int     `json:"total_count"`
 }
 
 type Order struct {
	 ObjectType         string          `json:docType`
	 EntityID           int             `json:"entity_id"`
	 CreatedAt          string          `json:"created_at"`
	 //CustomerEmail      string          `json:"customer_email"`
	 //CustomerFirstname  string          `json:"customer_firstname"`
	 CustomerID         int             `json:"customer_id"`
	 //CustomerLastname   string          `json:"customer_lastname"`
	 GlobalCurrencyCode string          `json:"global_currency_code"`
	 GrandTotal         float32         `json:"grand_total"`
	 IncrementId        string          `json:"increment_id"`
	 ShippingAmount     float32         `json:"shipping_amount"`
	 State              string          `json:"state"`
	 Status             string          `json:"status"`
	 Subtotal           float32         `json:"subtotal"`
	 totalorderCount    int             `json:"total_order_count"`
	 UpdatedAt          string          `json:"updated_at"`
	 orders             []order         `json:"orders"`
	 //BillingAddress     Address         `json:"billing_address"`
	 //Payment            Payment         `json:"payment"`
	 //StatusHistories    []StatusHistory `json:"status_histories"`
 }
 
 type order struct {
	 CreatedAt   string  `json:"created_at"`
	 Description string  `json:"description"`
	 orderID     int     `json:"order_id"`
	 Name        string  `json:"name"`
	 Price       float32 `json:"price"`
	 ProductID   int     `json:"product_id"`
	 QtyInvoiced int     `json:"qty_invoiced"`
	 QntOrdered  int     `json:"qty_ordered"`
	 Sku         string  `json:"sku"`
	 StoreId     int     `json:"store_id"`
	 UpdatedAt   string  `json:"updated_at"`
 }
 
 type Address struct {
	 AddressType string   `json:"address_type"`
	 City        string   `json:"city"`
	 Company     string   `json:"company"`
	 CountryID   string   `json:"country_id"`
	 Email       string   `json:"email"`
	 EntityID    int      `json:"entity_id"`
	 Firstname   string   `json:"firstname"`
	 Lastname    string   `json:"lastname"`
	 Postcode    string   `json:"postcode"`
	 Region      string   `json:"region"`
	 RegionCode  string   `json:"region_code"`
	 Street      []string `json:"street"`
	 Telephone   string   `json:"telephone"`
 }
 
 type Payment struct {
	 AccountStatus         string   `json:"account_status"`
	 AdditionalInformation []string `json:"additional_information"`
	 AmountOrdered         float32  `json:"amount_ordered"`
	 AmountPaid            float32  `json:"amount_paid"`
	 EntityID              int      `json:"entity_id"`
	 Method                string   `json:"method"`
	 ShippingAmount        float32  `json:"shipping_amount"`
 }
 
 type StatusHistory struct {
	 Comment    string `json:"comment"`
	 CreatedAt  string `json:"created_at"`
	 EntityID   int    `json:"entity_id"`
	 EntityName string `json:"entity_name"`
	 Status     string `json:"status"`
 }

 func (s *MagentoContract) Init(ctx contractapi.TransactionContextInterface, A string, Aval int, B string, Bval int) error {
	fmt.Println("MagentoContract Order Init")
	var err error
	fmt.Printf("Aval = %d, Bval = %d\n", Aval, Bval)
	err = ctx.GetStub().PutState(A, []byte(strconv.Itoa(Aval)))
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(B, []byte(strconv.Itoa(Bval)))
	if err != nil {
		return err
	}
	return nil
}


func (s *MagentoContract) InitOrders(ctx contractapi.TransactionContextInterface) error {
	fmt.Println("Calling magento orders api")
	request, _ := http.NewRequest(`GET`, apiHost+`orders?searchCriteria[pageSize]=100&searchCriteria[currentPage]=1&searchCriteria[sortOrders][0][field]=entity_id&searchCriteria[sortOrders][0][direction]=desc`, nil)
	request.Header.Set("Authorization", magentoAccessToken)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return fmt.Errorf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		// fmt.Println(string(data))
		var responseObject OrderResponse
		json.Unmarshal(data, &responseObject)
		for i := 0; i < len(responseObject.Orders); i++ {
			fmt.Println(responseObject.Orders[i].EntityID)
			responseObject.Orders[i].ObjectType = "orders"
			orderAsBytes, _ := json.Marshal(responseObject.Orders[i])
			key := "O" + strconv.Itoa(responseObject.Orders[i].EntityID)
			err = ctx.GetStub().PutState(key, orderAsBytes)
			if err != nil {
				return fmt.Errorf("failed to put to world state. %v", err)
			}
		}
	}
	return nil
}

func (s *MagentoContract) QueryOrder(ctx contractapi.TransactionContextInterface, orderID string) (string, error) {
	var err error
	key := "O" + orderID
	// Get the state from the ledger
	orderAsBytes, err := ctx.GetStub().GetState(key)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + key + "\"}"
		fmt.Println(err)
		return "", errors.New(jsonResp)
	}
	if orderAsBytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + key + "\"}"
		return "", errors.New(jsonResp)
	}
	fmt.Println(len(key))
	fmt.Println(len(orderAsBytes))

	fmt.Println(string(orderAsBytes))
	jsonResp := "{\"ID\":\"" + orderID + "\",\"order\":\"" + string(orderAsBytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return string(orderAsBytes), nil
}

func (s *MagentoContract) QueryAllOrders(ctx contractapi.TransactionContextInterface) (string, error) {
	startKey := "O0"
	endKey := "O999999"

	resultsIterator, err := ctx.GetStub().GetStateByRange(startKey, endKey)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for queryAllOrders\"}"
		fmt.Println(err)
		return "", errors.New(jsonResp)
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			jsonResp := "{\"Error\":\"resultsIterator\"}"
			return "", errors.New(jsonResp)
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString(`{"Key":`)
		buffer.WriteString(`"`)
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString(`"`)

		buffer.WriteString(`, "Record":`)
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString(`}`)
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString(`]`)

	fmt.Printf("- queryAllOrders:\n%s\n", buffer.String())

	return string(buffer.Bytes()), nil
}


func (s *MagentoContract) DeleteOrder(ctx contractapi.TransactionContextInterface, orderID string) error {
	key := "O" + orderID
	// Delete the key from the state in ledger
	err := ctx.GetStub().DelState(key)
	if err != nil {
		return fmt.Errorf("Failed to delete state")
	}
	return nil
}