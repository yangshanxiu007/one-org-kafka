package main

import (
    "github.com/hyperledger/fabric-contract-api-go/contractapi"
)

func main() {
    magentoContract := new(MagentoContract)

    cc, err := contractapi.NewChaincode(magentoContract)

    if err != nil {
        panic(err.Error())
    }

    if err := cc.Start(); err != nil {
        panic(err.Error())
    }
}