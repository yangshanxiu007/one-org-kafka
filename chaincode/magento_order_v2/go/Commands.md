Terminal 1
docker rm -f $(docker ps -aq) && docker volume prune
cd fabric-samples/chaincode-docker-devmode
docker-compose -f docker-compose-simple.yaml up

Terminal 2
cd fabric-samples/chaincode/magento_order_v2/go
/usr/local/go/bin/go mod init github.com/hyperledger/fabric-samples/chaincode/magento_order_v2/go
/usr/local/go/bin/go get -u github.com/hyperledger/fabric-contract-api-go

docker exec -it chaincode sh
cd magento_order_v2/go
go mod vendor
go build 
CORE_CHAINCODE_ID_NAME=order:0 CORE_PEER_TLS_ENABLED=false ./order -peer.address peer:7052

Terminal 3
docker exec -it cli sh 
peer chaincode install -p chaincodedev/chaincode/magento_order_v2/go -n order -v 0
peer chaincode instantiate -n order -v 0 -c '{"Args":[]}' -C myc

//Init for test
peer chaincode invoke -n order -c '{"Args":["Init","A","1","B","2"]}' -C myc

//Start Orders
peer chaincode invoke -n order -c '{"Args":["InitOrders"]}' -C myc

/Query Commands
peer chaincode invoke -n order -c '{"Args":["QueryOrder","28715"]}' -C myc
peer chaincode invoke -n order -c '{"Args":["QueryAllOrders"]}' -C myc

//Delete Commands
peer chaincode invoke -n order -c '{"Args":["DeleteOrder","28715"]}' -C myc






