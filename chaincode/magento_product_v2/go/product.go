/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The sample smart contract for documentation topic:
 * Writing Your First Blockchain Application
 */

package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	"fmt"
	"strconv"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"errors"
	"strings"
	"bytes"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

const (
	apiHost            = ""
	magentoAccessToken = ""
)


// Define the Smart Contract structure
type MagentoContract struct {
	contractapi.Contract
}

type ProductResponse struct {
	Items  []Item `json:"items"`
	Number int    `json:"total_count"`
}

//Item is the structure of the products stored in API
type Item struct {
	ObjectType    string  `json:"docType"`
	ID            int     `json:"id"`
	Sku           string  `json:"sku"`
	Name          string  `json:"name"`
	AtributeSetID int     `json:"attribute_set_id"`
	Price         float32 `json:"price"`
	Status        int     `json:"status"`
	Visibility    int     `json:"visibility"`
	TypeID        string  `json:"type_id"`
	CreatedAt     string  `json:"created_at"`
	UpdatedAt     string  `json:"updated_at"`
	// ProductLinks     string `json:"product_links"`
	// TierPrices       string `json:"tier_prices"`
	//CustomAttributes []Attribute `json:"custom_attributes"`
}

//Attribute refers to the custom attributes of the product
type Attribute struct {
	AttributeCode string `json:"attribute_code"`
	Value         string `json:"value"`
}

func (s *MagentoContract) Init(ctx contractapi.TransactionContextInterface, A string, Aval int, B string, Bval int) error {
	fmt.Println("MagentoContract Init")
	var err error
	fmt.Printf("Aval = %d, Bval = %d\n", Aval, Bval)
	err = ctx.GetStub().PutState(A, []byte(strconv.Itoa(Aval)))
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(B, []byte(strconv.Itoa(Bval)))
	if err != nil {
		return err
	}
	return nil
}


func (s *MagentoContract) InitProducts(ctx contractapi.TransactionContextInterface) error {
	fmt.Println("Calling magento product api")
	request, _ := http.NewRequest(`GET`, apiHost+`products?searchCriteria={"sortOrders":[{"field":"id","direction":"asc"}]}`, nil)
	request.Header.Set("Authorization", magentoAccessToken)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return fmt.Errorf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		// fmt.Println(string(data))
		var responseObject ProductResponse
		json.Unmarshal(data, &responseObject)
		for i := 0; i < len(responseObject.Items); i++ {
			fmt.Println(responseObject.Items[i].Sku)
			responseObject.Items[i].ObjectType = "product"
			itemAsBytes, _ := json.Marshal(responseObject.Items[i])
			key := "P" + strconv.Itoa(responseObject.Items[i].ID)
			err = ctx.GetStub().PutState(key, itemAsBytes)
			if err != nil {
				return fmt.Errorf("failed to put to world state. %v", err)
			}
		}
	}
	return nil
}

func (s *MagentoContract) QueryProduct(ctx contractapi.TransactionContextInterface, productID string) (string, error) {
	var err error
	key := "P" + productID
	// Get the state from the ledger
	itemAsBytes, err := ctx.GetStub().GetState(key)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + key + "\"}"
		fmt.Println(err)
		return "", errors.New(jsonResp)
	}
	if itemAsBytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + key + "\"}"
		return "", errors.New(jsonResp)
	}
	fmt.Println(len(key))
	fmt.Println(len(itemAsBytes))

	fmt.Println(string(itemAsBytes))
	jsonResp := "{\"ID\":\"" + productID + "\",\"sku\":\"" + string(itemAsBytes) + "\"}"
	fmt.Printf("Query Response:%s\n", jsonResp)
	return string(itemAsBytes), nil
}

/* uat attribute_set_id 21
*/
func (s *MagentoContract) CreateProduct(ctx contractapi.TransactionContextInterface, sku string, attribute_set_id string) error {
    var err error
	sku = strings.Replace(sku, " ", "+", -1)
	productBody := []byte(`
	{
		"product": {
		  "sku": "` + sku  + `",
		  "name": "` + sku  + `",
		  "attribute_set_id": "` + attribute_set_id  + `"
		}
	  }
	`)
	request, _ := http.NewRequest("POST", apiHost+"products", bytes.NewBuffer(productBody))
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", magentoAccessToken)
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return fmt.Errorf("The HTTP request failed with error. %v", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		var item Item
		json.Unmarshal(data, &item)

		if item.Sku != sku {
			fmt.Println("Invalid SKU")
			return fmt.Errorf("Invalid SKU")
		}
		item.ObjectType = "product"
		key := "P" + strconv.Itoa(item.ID)
		itemAsBytes, _ := json.Marshal(item)
		err = ctx.GetStub().PutState(key, itemAsBytes)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *MagentoContract) QueryAllProducts(ctx contractapi.TransactionContextInterface) (string, error) {
	startKey := "P0"
	endKey := "P999999"

	resultsIterator, err := ctx.GetStub().GetStateByRange(startKey, endKey)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for queryAllProducts\"}"
		fmt.Println(err)
		return "", errors.New(jsonResp)
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			jsonResp := "{\"Error\":\"resultsIterator\"}"
			return "", errors.New(jsonResp)
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString(`{"Key":`)
		buffer.WriteString(`"`)
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString(`"`)

		buffer.WriteString(`, "Record":`)
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString(`}`)
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString(`]`)

	fmt.Printf("- queryAllProducts:\n%s\n", buffer.String())

	return string(buffer.Bytes()), nil
}

func (s *MagentoContract) DeleteProduct(ctx contractapi.TransactionContextInterface, productID string) error {
	key := "P" + productID
	// Delete the key from the state in ledger
	err := ctx.GetStub().DelState(key)
	if err != nil {
		return fmt.Errorf("Failed to delete state")
	}
	return nil
}
