Terminal 1
docker rm -f $(docker ps -aq) && docker volume prune
cd fabric-samples/chaincode-docker-devmode
docker-compose -f docker-compose-simple.yaml up

Terminal 2
cd fabric-samples/chaincode/magento_product_v2/go
/usr/local/go/bin/go mod init github.com/hyperledger/fabric-samples/chaincode/magento_product_v2/go
/usr/local/go/bin/go get -u github.com/hyperledger/fabric-contract-api-go

docker exec -it chaincode sh
cd magento_product_v2/go
go mod vendor
go build 
CORE_CHAINCODE_ID_NAME=product:0 CORE_PEER_TLS_ENABLED=false ./product -peer.address peer:7052

Terminal 3
docker exec -it cli sh 
peer chaincode install -p chaincodedev/chaincode/magento_product_v2/go -n product -v 0
peer chaincode instantiate -n product -v 0 -c '{"Args":[]}' -C myc

//Init for test
peer chaincode invoke -n product -c '{"Args":["Init","A","1","B","2"]}' -C myc

//InitProducts Get all magento skus save to fabric
peer chaincode invoke -n product -c '{"Args":["InitProducts"]}' -C myc

//Query product with ID
peer chaincode invoke -n product -c '{"Args":["QueryProduct","8"]}' -C myc

//Query all products
peer chaincode invoke -n product -c '{"Args":["QueryAllProducts"]}' -C myc

//Delete product from fabric with ID
peer chaincode invoke -n product -c '{"Args":["DeleteProduct","8"]}' -C myc

//Create product on magento save to fabric with sku's name and attribute set ID
peer chaincode invoke -n product -c '{"Args":["CreateProduct","test333","21"]}' -C myc

