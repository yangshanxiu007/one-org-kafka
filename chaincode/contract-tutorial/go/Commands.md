Terminal 1
docker rm -f $(docker ps -aq) && docker volume prune
cd fabric-samples/chaincode-docker-devmode
docker-compose -f docker-compose-simple.yaml up

Terminal 2
cd fabric-samples/chaincode/contract-tutorial/go
/usr/local/go/bin/go mod init github.com/hyperledger/fabric-samples/chaincode/contract-tutorial/go
/usr/local/go/bin/go get -u github.com/hyperledger/fabric-contract-api-go

docker exec -it chaincode sh
cd contract-tutorial/go
go mod vendor
go build -o complex
CORE_CHAINCODE_ID_NAME=complex:0 CORE_PEER_TLS_ENABLED=false ./complex -peer.address peer:7052

Terminal 3
docker exec -it cli sh
peer chaincode install -p chaincodedev/chaincode/contract-tutorial/go -n complex -v 0
peer chaincode instantiate -n complex -v 0 -c '{"Args":[]}' -C myc

//new
#SimpleContract
peer chaincode invoke -n complex -c '{"Args":["org.example.com.SimpleContract:Create", "KEY_3", "VALUE_1"]}' -C myc

peer chaincode invoke -n complex -c '{"Args":["org.example.com.SimpleContract:Update", "KEY_3", "VALUE_2"]}' -C myc

peer chaincode query -n complex -c '{"Args":["org.example.com.SimpleContract:Read", "KEY_3"]}' -C myc

#ComplexContract
peer chaincode invoke -n complex -c '{"Args":["org.example.com.ComplexContract:NewAsset", "ASSET_1", "{\"forename\": \"green\", \"surname\": \"jasmine\"}", "1"]}' -C myc

peer chaincode invoke -n complex -c '{"Args":["org.example.com.ComplexContract:UpdateOwner", "ASSET_1", "{\"forename\": \"green\", \"surname\": \"conga\"}"]}' -C myc

peer chaincode invoke -n complex -c '{"Args":["org.example.com.ComplexContract:UpdateValue", "ASSET_1", "300"]}' -C myc

peer chaincode query -n complex -c '{"Args":["org.example.com.ComplexContract:GetAsset", "ASSET_1"]}' -C myc

//old
//New Asset
peer chaincode invoke -n complex -c '{"Args":["NewAsset"]}' -C myc

/Update Owner
peer chaincode invoke -n complex -c '{"Args":["UpdateOwner","28715"]}' -C myc

//Get Asset
peer chaincode invoke -n complex -c '{"Args":["GetAsset","28715"]}' -C myc